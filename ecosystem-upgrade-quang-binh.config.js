module.exports = {
  apps: [
    {
      name: 'iLotusland_quang_binh_TXT',
      script: './src/app.js',
      exp_backoff_restart_delay: 100,
      restart_delay: 3000,
      env: {
        NODE_ENV: 'production',
        ROOT_WATCH: '/Users/mac/Documents/Project/VIET_AN/iLotusland/back-end/watch-onpremise/FTP',
        TYPE: 'TXT',
        IS_CHANGE_NAME: true,
        IS_MOVE: true,
        MOVE_FOLDER: '/Users/mac/Documents/Project/VIET_AN/iLotusland/back-end/watch-onpremise/FTP_IMPORT',

        DB_ADMIN_URI: 'mongodb://18.140.67.241:27017/ilotusland_admin_dev',
        DB_ADMIN_USER: 'root',
        DB_ADMIN_PASS: '8637f002b96e7eee58d840b9291ad8ac',

        DB_ORG_URI: 'mongodb://18.140.67.241:27017/ilotusland_stnmt_quangbinh',
        DB_ORG_NAME: 'ilotusland_stnmt_quangbinh',
        DB_ORG_USER: 'root',
        DB_ORG_PASS: '8637f002b96e7eee58d840b9291ad8ac',

        CATEGORY_API: 'https://test-api.ilotusland.asia',
        HOST_FCM: 'https://test-api.ilotusland.asia',
        FCM_SECRET: '506b04d39d168bbfa3725221c1522d0',
        SERVICE_MONITORING: 'https://test-api.ilotusland.asia/data-monitoring',
      },
    },
    
  ],
};
