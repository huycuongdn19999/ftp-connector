module.exports = {
  apps: [
    {
      name: 'iLotusland_print_nodewatch_TXT',
      script: './src/app.js',
      exp_backoff_restart_delay: 100,
      restart_delay: 3000,
      env: {
        NODE_ENV: 'production',
        ROOT_WATCH: '/Users/mac/Documents/Project/VIET_AN/iLotusland/back-end/watch-onpremise/FTP_IMPORT',
        TYPE: 'TXT',
        IS_CHANGE_NAME: true,
        IS_MOVE: true,
        MOVE_FOLDER: '/Users/mac/Documents/Project/VIET_AN/iLotusland/back-end/watch-onpremise/FTP_IMPORTED',

        DB_ADMIN_URI: 'mongodb://192.168.1.16:27017/ilotusland_admin_dev',
        DB_ADMIN_USER: 'root',
        DB_ADMIN_PASS: '8637f002b96e7eee58d840b9291ad8ac',

        DB_ORG_URI: 'mongodb://192.168.1.16:27017/ilotusland_thep_miennam',
        DB_ORG_NAME: 'ilotusland_thep_miennam',
        DB_ORG_USER: 'root',
        DB_ORG_PASS: '8637f002b96e7eee58d840b9291ad8ac',

        
        CATEGORY_API: 'https://quantrac-api.thepmiennam.com.vn',
        HOST_FCM: 'https://quantrac-api.thepmiennam.com.vn',
        FCM_SECRET: '8637f002b96e7eee58d840b9291ad8ac',
        SERVICE_MONITORING: 'https://quantrac-api.thepmiennam.com.vn/data-monitoring',
      },
    },
  ],
};
