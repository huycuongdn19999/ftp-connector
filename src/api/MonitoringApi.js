const { SERVICE_MONITORING } = require('./../config');
const fetch = require('./../utils/fetch');

const repleaceMeasureLogApi = async ({ minLimit, maxLimit, minTend, maxTend, value }) => {
  return fetch.postFetch(`${SERVICE_MONITORING}/replace-measurelog`, {
    measureLog: { minLimit, maxLimit, minTend, maxTend, value },
  });
};
const checkAndPushNotification = async ({ organization, station, data }) => {
  // console.log(station, "---1---")
  // console.log(data, "---2---")
  return fetch.postFetch(`${SERVICE_MONITORING}/check-push-notification`, { organization, station, data });
};

module.exports = {
  repleaceMeasureLogApi,
  checkAndPushNotification,
};
