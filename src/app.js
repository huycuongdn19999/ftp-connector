const _ = require('lodash');
require('colors');
const schedule = require('node-schedule');

// const { WathmanCustom } = require('./lib/watchman');
const { Stack } = require('./lib/Stack');
const { readAndPushDataToBroker } = require('./lib/sv24/readFile');

// const StationAuto = require('./models/StationAuto')
const watch = require('node-watch');
const { ROOT_WATCH, TYPE } = require('./config');

const rootFTP = ROOT_WATCH;
const typeFile_toUpper = _.toUpper(TYPE);

const stack = new Stack();

main();
async function main() {
  watch(
    rootFTP,
    {
      recursive: true,
      filter: (f) => {
        // console.log('run 1', typeFile_toUpper);
        if (typeFile_toUpper == 'TXT') {
          return /^.*\.txt$/.test(f) || /^.*\.TXT$/.test(f);
        } else if (typeFile_toUpper == 'CSV') {
          return /^.*\.csv$/.test(f) || /^.*\.CSV$/.test(f);
        } else if (typeFile_toUpper == 'JSON') {
          return /\.json$/;
        }
      },
    },
    function (evt, fileNameWithPath) {
      if (evt == 'remove') {
        return;
      }
      console.log(`file changed: ${fileNameWithPath}`.green);
      if (!stack.checkCoChua(fileNameWithPath)) {
        console.log('lamviec', stack.length);
      } else {
        console.log('k0 co lam', stack.length);
      }
    }
  );

  schedule.scheduleJob('* * * * * *', async () => {
    try {
      console.log(`Xử lý File Starting !!! stack:${stack.length}`.green);
      if (stack.length > 0) lamviec(stack.shift());
    } catch (e) {
      console.log(`Đây là lỗi: ${JSON.stringify(e)}`.red);
    }
  });
}

async function lamviec(fileNameWithPath = '') {
  let fileNames = fileNameWithPath.split('/');
  if (fileNames.length < 2) fileNames = fileNameWithPath.split('\\');
  const fileLastName = fileNames[fileNames.length - 1];
  // console.log(fileLastName, '==fileLastName==');
  readAndPushDataToBroker({ filename: fileNameWithPath });
}
