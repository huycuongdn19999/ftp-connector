// const moveFile = require('../sv24/moveFile')
const fs = require('fs');
const { isNumber } = require('lodash');
const moment = require('moment');
const moveFile = require('../sv24/moveFile');

module.exports = async ({ filename, showLog, fileName }) => {
  try {
    let data = await fs.readFileSync(filename, 'utf8');
    let receivedAt = '';
    const measureData = data
      .split(`\n`)
      .map((row, index) => {
        /*eslint-disable*/
        if (row.trim() !== '') {
          let rowValues = row.replace(`/[^\w\s]/gi`, ' ').replace(/\t/g, '[khoangTrang]').split('[khoangTrang]');
          
          const _RowData = [];
          if (rowValues.length === 6) {
            
            _RowData[0] = `${rowValues[0]}_${rowValues[1]}`;
            _RowData[1] = rowValues[2];
            _RowData[2] = rowValues[3];
            _RowData[3] = rowValues[4];
            _RowData[4] = rowValues[5];
            rowValues = _RowData;
          }else if(rowValues.length === 4 && index === 0){ // nếu chỉ trả về 4 giá trị trong 1 row thì chỉ áp dụng đc với trạm có 1 chỉ tiêu
            const arrKey = fileName.split('_')
            _RowData[0] = arrKey.pop(); // get key chi tieu
            _RowData[1] = rowValues[1];// Value
            _RowData[2] = rowValues[2];// Unit
            _RowData[3] = rowValues[0];// Time
            _RowData[4] = rowValues[3];// Status
            rowValues = _RowData;
          }

          const receivedTime = moment(rowValues[3], 'YYYYMMDDHHmmss');
          /* eslint-enable */
          if (!receivedTime.isValid() || rowValues[3].length != 14) {
            moveFile({ filename: filename, type: 'ERROR' });
            throw new Error('Time not valid');
          }
          if (rowValues[0].includes(' ')) {
            moveFile({ filename: filename, type: 'ERROR' });
            throw new Error('Key not [Khoang trang]');
          }
          if (isNaN(rowValues[1]) ) {
            moveFile({ filename: filename, type: 'ERROR' });
            throw new Error('Value not number');
          }
          if (isNaN(rowValues[4])) {
            moveFile({ filename: filename, type: 'ERROR' });
            throw new Error('status not number');
          }

          if (receivedAt === '') {
            receivedAt = receivedTime.toDate();
          }

          return {
            measureName: rowValues[0],
            value: Number.parseFloat(rowValues[1]),
            unit: rowValues[2],
            time: receivedTime.toDate(),
            statusDevice: Number.parseInt(rowValues[4])
          };
        }
        return null;
      })
      .filter((measuring) => !!measuring);
    if (showLog) {
      console.info(`${filename} readed`);
    }
    if (!receivedAt) {
      moveFile({ filename: filename, type: 'ERROR' });
      throw new Error('Time not valid');
    }
    return {
      measureData,
      receivedAt,
    };
  } catch (e) {
    // moveFile({ filename, type: 'error' })
    moveFile({ filename: filename, type: 'ERROR' });
    console.error(`${filename} not read`, e);
    return { error: true };
  }
};
