// const moveFile = require('../sv24/moveFile')
const fs = require('fs');
const moveFile = require('../sv24/moveFile');

module.exports = async ({ filename, showLog, fileName }) => {
  try {
    const buffer = fs.readFileSync(filename, 'utf8');
    if (showLog) {
      console.info(`${filename} readed`);
    }
    return { data: JSON.parse(buffer) };
  } catch (e) {
    // moveFile({ filename, type: 'error' })
    moveFile({ filename: filename, type: 'ERROR' });
    console.error(`${filename} not read`, e);
    return { error: true };
  }
};
