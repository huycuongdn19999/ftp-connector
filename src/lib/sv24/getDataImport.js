const moment = require('moment')
const _ = require('lodash')
const warningLevels = require('../../constants/warningLevels.js')

function repleaceMeasureLog ({ warningConfigs, measureLog }) {
  measureLog.warningLevel = null
  // check vuot nguong va chuan bi vuot nguong
  if (measureLog && measureLog.value && measureLog.maxLimit) {
    // Check vuot nguong
    if (measureLog.value >= measureLog.maxLimit) {
      measureLog.warningLevel = warningLevels.EXCEEDED
    } else if (
      measureLog.value / measureLog.maxLimit * 100 >=
      warningConfigs.exceededPreparing.value
    ) {
      // Check chuan bi vuot
      measureLog.warningLevel = warningLevels.EXCEEDED_PREPARING
    }
  }

  return measureLog
}

module.exports = function getDataImport ({
  station,
  receivedAt,
  measureData,
  warningConfigs
}) {
  let dataImport = {
    receivedAt: receivedAt,
    measuringLogs: {}
  }

  let isRemove = false
  const approveLogs = { measuringLogs: {} }
  const removeLogs = { measuringLogs: {} }

  // console.log(station.measuringList, measureData)

  station.measuringList.forEach(item => {
    let measureFinded = measureData.find(
      measureSoure => {
        // console.log(measureSoure.measureName,item.unit)
        return measureSoure.measureName === item.measuringSrc
      }
    )

    if (measureFinded) {
      const value = item.ratio ? measureFinded.value * item.ratio : measureFinded.value
      approveLogs.measuringLogs[item.measuringDes] = { approvedValue: value, hasApproved: false }
      removeLogs.measuringLogs[item.measuringDes] = { removeValue: value }



      dataImport.measuringLogs[item.measuringDes] = repleaceMeasureLog({
        warningConfigs,
        measureLog: {
          value,
          minLimit: item.minLimit,
          maxLimit: item.maxLimit,
          minTend: item.minTend,
          maxTend: item.maxTend,
          unit: item.unit,
          statusDevice: measureFinded.statusDevice
        }
      })
    }
  })

  if (isRemove) {
    return _.merge(dataImport, removeLogs)
  } else {
    return _.merge(dataImport, approveLogs)
  }

  // return dataImport
}
