const dataStationAutoDao = require('../../dao/dataStationAutoDao');
const logDataStationAutoDao = require('../../dao/logDataStationAutoDao');

const stationAutoDao = require('../../dao/stationAutoDao');

module.exports = async function importData({ stationKey, data: { receivedAt, measuringLogs } }) {
  try {
    logDataStationAutoDao.createOrUpdate(
      {
        receivedAt,
        measuringLogs,
      },
      stationKey
    );

    dataStationAutoDao.createOrUpdate(
      {
        receivedAt,
        measuringLogs,
      },
      stationKey
    );
    const resStationAuto = await stationAutoDao.DAOupdateLastLog(stationKey, { receivedAt, measuringLogs });
    return { success: true, data: resStationAuto };
  } catch (e) {
    return { error: true, message: e.message };
  }
};
