const path = require('path');
const fs = require('fs');
const config = require('../../config');
const moment = require('moment');

module.exports = async function moveFile({ filename, type }) {
  console.log('Move file');
  let tamp = filename.split('/');
  if (tamp.length < 2) tamp = filename.split('\\');
  let pathFile = tamp[tamp.length - 1];

  const dateStr = moment().format('YYYYMMDD');

  const desPath = `${config.MOVE_FOLDER}/${dateStr}`;
  let newImportPath = `${desPath}/${pathFile}`;

  if (!fs.existsSync(desPath)) {
    fs.mkdirSync(desPath, { recursive: true });
  }

  fs.renameSync(filename, newImportPath);
};
