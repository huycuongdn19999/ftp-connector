const fs = require('fs');
const path = require('path');
const moment = require('moment');
const _ = require('lodash');
const moveFile = require('./moveFile');
const readFileType = require('../readFileType');
const { pushNotification } = require('./pushNotification');
const { WEBHOOK_URL, IS_MOVE, TYPE } = require('../../config');
const { postFetch } = require('./../../utils/fetch');

const FILE_TYPE = readFileType.FILE_TYPE;

async function changeFileName(filename) {
  let newFileName = path.join(path.dirname(filename), 'IMPORTED_' + path.basename(filename));
  await fs.renameSync(filename, newFileName);
  return newFileName;
}

async function readAndPushDataToBroker({ filename }) {
  const readFileResult = await readFileType({
    fileType: TYPE,
    filename,
    // fileName: station.fileName,
    showLog: false,
  });
  if (readFileResult.error) {
    return false;
  }
  // console.log(readFileResult.data, '==push with data');
  const res = await postFetch(WEBHOOK_URL, { data: readFileResult.data });
  console.log({ res, IS_MOVE });
  if (res.success && IS_MOVE) {
    moveFile({ filename });
  }
}
module.exports = {
  readAndPushDataToBroker,
  changeFileName,
};
// module.exports.changeFileName = changeFileName;
