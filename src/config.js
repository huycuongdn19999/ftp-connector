if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

function toBoolean(val) {
  let result = false;
  if (val === 'true' || val === 1) result = true;
  return result;
}
var toBoolean = require('to-boolean');

module.exports = {
  ROOT_WATCH: process.env.ROOT_WATCH,
  TYPE: process.env.TYPE ? process.env.TYPE : 'TXT',
  MOVE_FOLDER: process.env.MOVE_FOLDER,
  IS_MOVE: toBoolean(process.env.IS_MOVE),
  IS_CHANGE_NAME: toBoolean(process.env.IS_CHANGE_NAME),

  DB_ADMIN_URI: process.env.DB_ADMIN_URI,
  DB_ADMIN_USER: process.env.DB_ADMIN_USER,
  DB_ADMIN_PASS: process.env.DB_ADMIN_PASS,

  DB_ORG_URI: process.env.DB_ORG_URI,
  DB_ORG_NAME: process.env.DB_ORG_NAME,
  DB_ORG_USER: process.env.DB_ORG_USER,
  DB_ORG_PASS: process.env.DB_ORG_PASS,

  CATEGORY_API: process.env.CATEGORY_API,
  FCM_API: process.env.HOST_FCM,
  FCM_SECRET: process.env.FCM_SECRET,
  SERVICE_MONITORING: process.env.SERVICE_MONITORING,
  WEBHOOK_URL: process.env.WEBHOOK_URL,
};
