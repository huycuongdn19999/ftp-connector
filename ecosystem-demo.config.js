module.exports = {
  apps: [
    {
      name: 'iLotusland_demo_nodewatch_TXT',
      script: './src/app.js',
      exp_backoff_restart_delay: 100,
      restart_delay: 3000,
      env: {
        NODE_ENV: 'production',
        ROOT_WATCH: 'C:/FTP/ENV_DEMO/FTP',
        TYPE: 'TXT',
        IS_CHANGE_NAME: true,
        IS_MOVE: true,
        MOVE_FOLDER: 'C:/FTP/ENV_DEMO/FTP_IMPORT',

        DB_ADMIN_URI: 'mongodb://54.254.171.126:27017/ilotusland_admin_dev',
        DB_ADMIN_USER: 'dev',
        DB_ADMIN_PASS: '506b04d39d168bbfa3725221c1522d04',

        DB_ORG_URI: 'mongodb://54.254.171.126:27017/ilotusland_Vietan',
        DB_ORG_NAME: 'ilotusland_Vietan',
        DB_ORG_USER: 'dev',
        DB_ORG_PASS: '506b04d39d168bbfa3725221c1522d04',

        CATEGORY_API: 'https://demo-api.ilotusland.asia',
        HOST_FCM: 'https://demo-api.ilotusland.asia',
        FCM_SECRET: 'FCM_SECRET',
        SERVICE_MONITORING: 'https://demo-api.ilotusland.asia/data-monitoring',
      },
    },
    
  ],
};
